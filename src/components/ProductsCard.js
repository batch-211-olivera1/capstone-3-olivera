import { Link } from 'react-router-dom';
import { Card, Button, Col, Row } from 'react-bootstrap';

export default function ProductsCard({productProp}){

    let { name, description, price, _id } = productProp;


    return(
    <Col className="cardContainer col-6">
        <Card className="p-3 mb-3">
            <Card.Body className="card-dark">
                <Card.Title>{name}</Card.Title>
                <Card.Subtitle>Description:</Card.Subtitle>
                <Card.Text>{description}</Card.Text>
                <Card.Subtitle>Price:</Card.Subtitle>
                <Card.Text>PHP {price}</Card.Text>
            <Button as={Link} to={`/products/search/${_id}`}>Buy now</Button>
            </Card.Body>
        </Card>       
    </Col>
   
    )
}