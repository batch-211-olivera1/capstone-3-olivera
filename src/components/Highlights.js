import {Row, Col, Carousel} from 'react-bootstrap';
import { useState } from "react";

export default function Highlights(){

	 const [index, setIndex] = useState(0);

  const handleSelect = (selectedIndex, e) => {
    setIndex(selectedIndex);
    };
	return(
	
    <Carousel activeIndex={index} onSelect={handleSelect}>
      <Carousel.Item>
        <img
          className="d-block w-100"
          src="https://bgr.com/wp-content/uploads/2022/09/apple-iphone-14-event-124.jpg?resize=1536,855"
          alt="First slide"
        />
        <Carousel.Caption>
          <h3>iPhone 14 and iPhone 14 Plus</h3>
          <p>iOS 16 lets you customize your Lock Screen in fun new ways. Layer a photo to make it pop. Track your Activity rings. And see live updates from your favorite apps.</p>
        </Carousel.Caption>
      </Carousel.Item>
      <Carousel.Item>
        <img
          className="d-block w-100"
          src="https://cdn.opstatics.com/store/20170907/assets/images/events/instant-noodle/19811/pics/full/3.webp"
          alt="Second slide"
        />

        <Carousel.Caption>
          <h3>OnePlus 8 Pro</h3>
          <p>120 Hz Fluid Display Qualcomm® Snapdragon™ 865 and 5G Warp Charge 30 Wireless 48 MP Quad Camera.</p>
        </Carousel.Caption>
      </Carousel.Item>
      <Carousel.Item>
        <img
          className="d-block w-100"
          src="https://th.bing.com/th/id/OIP.xAXcvL92rsRVJMEpBpWWKwHaE4?pid=ImgDet&rs=1"
          alt="Third slide"
        />

        <Carousel.Caption>
          <h3>Samsung Galaxy S21 5G</h3>
          <p>
            Make your holiday awesome with the new Samsung Galaxy s21.
          </p>
        </Carousel.Caption>
      </Carousel.Item>
    </Carousel>
	
 )
}
