import { useState, useEffect, useContext } from 'react';
import { Container, Card, Modal, Button, Row, Col } from 'react-bootstrap';
import UserContext from '../UserContext';
import { useParams, Link, useNavigate } from 'react-router-dom';
import Swal from 'sweetalert2';

export default function ProductView(){

	const { user } = useContext(UserContext);

	const navigate = useNavigate();

	const { productId } = useParams();
	const [name, setName] = useState("");
	const [description,setDescription] = useState("");
	const [price,setPrice] = useState (0);
	const [bodyProductId, setBodyProductId] = useState("");
	const [bodyQuantity, setBodyQuantity] = useState(0);
	const [isActive, setIsActive] = useState(false);
	const [isZero, setIsZero] = useState(0);

	const checkout = () => {
		const newTotalAmount = price * bodyQuantity
		fetch(`${process.env.REACT_APP_API_URL}/orders/checkout`,{
			method: 'POST',
			headers:{
				'Content-Type':'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
                   	productId: productId,
					quantity: bodyQuantity
			})
		})
		.then(res=>res.json())
		.then(data=>{
			if(data===true){
				Swal.fire({
					title: "Successful!",
            		icon: "success",
            		text: "Thank you for ordering"
				})
				navigate("/products");
			}else{
				Swal.fire({
					title: "Something went wrong",
            		icon: "error",
            		text: "Check credentials"
				})
			}
		})
	}
	useEffect(()=>{
		console.log(productId)
		fetch(`${process.env.REACT_APP_API_URL}/products/search/${productId}`)
		.then(res=>res.json())
		.then(data=>{
			setName(data.name);
			setDescription(data.description);
			setPrice(data.price);
		})
	})

	useEffect(() => {

        if(bodyQuantity <= 0){
            setIsActive(false)
            setIsZero(true)
        }else{
            setIsActive(true);
            setIsZero(false)
        }

    });


	return(
		<Container className="mt-5">
			<Row>
				<Col lg={{span:6, offset:3}}>
					<Card>
						<Card.Body className="formContainer">
							<Card.Title>{name}</Card.Title>
							<Card.Subtitle>Description:</Card.Subtitle>
							<Card.Text>{description}</Card.Text>
							<Card.Subtitle>Price:</Card.Subtitle>
							<Card.Text>{price}</Card.Text>
							<div>
							<Card.Subtitle>Quantity:</Card.Subtitle>
							{
                                isZero
                                ?
                                <Button className="counter" variant="secondary" onClick={()=>setBodyQuantity(bodyQuantity-1)} disabled>-</Button>
                                            :
                                <Button className="counter" variant="secondary" onClick={()=>setBodyQuantity(bodyQuantity-1)}>-</Button>
                            }


                            <span className="result-data">{bodyQuantity}</span>
                                        <Button className="counter" variant="secondary" onClick={()=>setBodyQuantity(bodyQuantity+1)}>+</Button>
                            </div>
							<div className="mt-3">
							<Card.Subtitle>Total Amount:</Card.Subtitle>
							<Card.Text><span className="result-data">{bodyQuantity*price}</span></Card.Text>
							{
								(user.id!==null)?
								<Button variant="primary" onClick={()=>checkout(bodyProductId)}>checkout</Button>
								:
								<Link className="btn btn-danger" as={Link} to="/login">Please Login first</Link>
							}
							</div>

						</Card.Body>
					</Card>
				</Col>
			</Row>
		</Container>
		)
}
