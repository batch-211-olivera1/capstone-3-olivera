
import { useState, useContext } from 'react';
import {Navbar, Nav, Container} from 'react-bootstrap';
import { Link } from 'react-router-dom';
import UserContext from '../UserContext';

export default function AppNavbar(){
  
  const { user } = useContext(UserContext);
  
  return (
    <Navbar className="nav-bg" expand="lg">
      <Container>
        <Navbar.Brand as={Link} to="/">Smuggle</Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="navbar me-auto">
            <Nav.Link as={Link} to="/">Home</Nav.Link>
            <Nav.Link as={Link} to="/products">Phones</Nav.Link>
            {(user.id !== null)
            ?
             <Nav.Link as={Link} to="/logout">Logout</Nav.Link>
            :
            <>
            <Nav.Link as={Link} to="/login">Login</Nav.Link>
            <Nav.Link as={Link} to="/register">Register</Nav.Link>
            </>
            }
          </Nav>
        </Navbar.Collapse>
      </Container>
    </Navbar>
  );
}





