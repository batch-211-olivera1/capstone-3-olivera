import {Container} from 'react-bootstrap';
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import Home from './pages/Home';
import Register from './pages/Register';
import Products from './pages/Products';
import Login from './pages/Login';
import Logout from './pages/Logout';
import Error from './pages/Error';
import AppNavbar from './components/AppNavbar';
import Dashboard from './components/Dashboard';
import ProductsView from './components/ProductsView';
import AddProduct from './components/AddProduct';
import UpdateProduct from './components/UpdateProduct';
import UserContext from './UserContext';
import { UserProvider } from './UserContext';
import { useState, useEffect } from 'react';
import './App.css';

function App() {
   const [user, setUser] = useState({
    id: null,
    isAdmin: null
  })

   const unsetUser = () => {
    localStorage.clear();
  }

  useEffect(()=>{
    fetch(`${process.env.REACT_APP_API_URL}/users/details`,{
      headers:{
        Authorization: `Bearer ${localStorage.getItem('token')}`
      }
    })
    .then(res=>res.json())
    .then(data=>{
      // console.log(data);

      if(typeof data._id !== "undefined"){
        setUser({
          id: data._id,
          isAdmin: data.isAdmin
        });
      }else{
        setUser({
          id: null,
          isAdmin: null
        })
      }
    })
  },[]);

  return (

      <UserProvider value = {{user, setUser, unsetUser}}>
        <Router>
          <AppNavbar/>
          <Container>
            <Routes>   
              <Route path="/" element={<Home/>}/>
              <Route path="/dashboard" element={<Dashboard/>}/>
              <Route path="/addProduct" element={<AddProduct/>} />
              <Route path="/updateProduct/:productId" element={<UpdateProduct/>} />
              <Route path="/products" element={<Products/>}/>
              <Route path="/products/search/:productId" element={<ProductsView/>}/> 
              <Route path="/login" element={<Login/>}/>
              <Route path="/register" element={<Register/>}/>
              <Route path="/logout" element={<Logout/>}/>
              <Route path="*" element={<Error/>}/>
            </Routes>
          </Container>
        </Router>
      </UserProvider>
  );
}

export default App;