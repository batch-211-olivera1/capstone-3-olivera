import { useEffect, useState, useContext } from 'react';
import { Navigate } from 'react-router-dom';
import ProductsCard from '../components/ProductsCard';
import UserContext from '../UserContext';


export default function Products(){

		const [products, setProducts] =useState([])

	
	const {user} = useContext(UserContext);
	console.log(user);

	
	useEffect(()=>{
		
	fetch(`${process.env.REACT_APP_API_URL}/products/activeProducts`)
	.then(res=>res.json())
	.then(data=>{

		
	const productArr = (data.map(product => {
		
		return (
			
			<ProductsCard productProp={product} key={product._id}/>
			)
		}))
		setProducts(productArr)
	})

	},[])

	
	return(
		(user.isAdmin)?
		<Navigate to="/dashboard"/>
		:
		<>
		<div className="mt-5 mb-3">
			<h1>Products</h1>
		</div>
			{products}
		</>
	)
}