import Banner from '../components/Banner';
import Highlights from '../components/Highlights';


export default function Home(){
const data = {
		title: "Welcome to Smuggle",
		description: "Find your new Phone",
		destination: "/products",
		label: "View products"
	}

	
	return(
		<>
			<Banner bannerProp={data}/>
			<Highlights/>
		</>
	)
}